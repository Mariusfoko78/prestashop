<?php
/* Smarty version 3.1.43, created on 2022-12-12 10:06:58
  from 'module:pscheckoutviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6396ef328ad032_30875432',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd97ff8323a9e1562670fa52cdd3acda88bde138' => 
    array (
      0 => 'module:pscheckoutviewstemplatesh',
      1 => 1669024613,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6396ef328ad032_30875432 (Smarty_Internal_Template $_smarty_tpl) {
?>

<form id="ps_checkout-hosted-fields-form" class="form-horizontal">
  <div class="form-group">
    <label class="form-control-label" for="ps_checkout-hosted-fields-card-number"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Card number','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</label>
    <div id="ps_checkout-hosted-fields-card-number" class="form-control">
      <div id="card-image">
        <img class="defautl-credit-card" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/credit_card.png" alt="">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group col-xs-6 col-6">
      <label class="form-control-label" for="ps_checkout-hosted-fields-card-expiration-date"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Expiry date','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</label>
      <div id="ps_checkout-hosted-fields-card-expiration-date" class="form-control"></div>
    </div>
    <div class="form-group col-xs-6 col-6">
      <label class="form-control-label" for="ps_checkout-hosted-fields-card-cvv"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'CVC','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</label>
      <div class="ps_checkout-info-wrapper">
        <div class="ps_checkout-info-button" onmouseenter="cvvEnter()" onmouseleave="cvvLeave()">i
          <div class="popup-content" id="cvv-popup">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modulePath']->value, ENT_QUOTES, 'UTF-8');?>
views/img/cvv.svg" alt="">
              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The security code is a','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
 <b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'3-digits','mod'=>'ps_checkout'),$_smarty_tpl ) );?>
</b> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'code on the back of your credit card. In some cases, it can be 4-digits or on the front of your card.','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

          </div>
        </div>
      </div>
      <div id="ps_checkout-hosted-fields-card-cvv" class="form-control"></div>
    </div>
  </div>
  <div id="payments-sdk__contingency-lightbox"></div>
</form>
<?php echo '<script'; ?>
>
  function cvvEnter() {
    var popup = document.getElementById("cvv-popup");
    popup.classList.add("show");
  }
  function cvvLeave() {
    var popup = document.getElementById("cvv-popup");
    popup.classList.remove("show");
  }
<?php echo '</script'; ?>
>
<?php }
}
