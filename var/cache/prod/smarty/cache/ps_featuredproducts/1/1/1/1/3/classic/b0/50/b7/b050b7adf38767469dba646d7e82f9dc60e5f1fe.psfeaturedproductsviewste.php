<?php
/* Smarty version 3.1.43, created on 2022-12-12 14:53:54
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_639732728a7b88_70812197',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1669023541,
      2 => 'module',
    ),
    '985fd1d5c467493d95da5f07fc48d945fd32e1c5' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1669023538,
      2 => 'file',
    ),
    '83dd6d762f92abec44acc92522c8360f5dd88507' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1669023538,
      2 => 'file',
    ),
    'ca65dc4bb3793545c91c6eb761860cc05e494f0d' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1669023538,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_639732728a7b88_70812197 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Produits populaires
  </h2>
  

<div class="products row">
            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="11" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/11-sweat-shirt-.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/28-home_default/sweat-shirt-.jpg"
                alt="casquette"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/28-large_default/sweat-shirt-.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/11-sweat-shirt-.html" content="http://localhost/prestashop/fr/accueil/11-sweat-shirt-.html">casquette</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  18,15 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="11" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="13" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/13-sweat-capuche.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/31-home_default/sweat-capuche.jpg"
                alt="sweat"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/31-large_default/sweat-capuche.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/13-sweat-capuche.html" content="http://localhost/prestashop/fr/accueil/13-sweat-capuche.html">sweat</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  24,20 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="13" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="9" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/9-sweat-shirt-capuche-blanc.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/15-home_default/sweat-shirt-capuche-blanc.jpg"
                alt="sweat-shirt capuche blanc"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/15-large_default/sweat-shirt-capuche-blanc.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/9-sweat-shirt-capuche-blanc.html" content="http://localhost/prestashop/fr/accueil/9-sweat-shirt-capuche-blanc.html">sweat-shirt capuche blanc</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  48,40 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="9" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/8-sweat-shirt-capuche-pour-homme.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/14-home_default/sweat-shirt-capuche-pour-homme.jpg"
                alt="sweat-shirt capuche pour homme"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/14-large_default/sweat-shirt-capuche-pour-homme.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/8-sweat-shirt-capuche-pour-homme.html" content="http://localhost/prestashop/fr/accueil/8-sweat-shirt-capuche-pour-homme.html">sweat-shirt capuche pour homme</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  54,45 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="8" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/7-sweat-shirt-a-capuche-homme.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/12-home_default/sweat-shirt-a-capuche-homme.jpg"
                alt="sweat-shirt à capuche homme"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/12-large_default/sweat-shirt-a-capuche-homme.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/7-sweat-shirt-a-capuche-homme.html" content="http://localhost/prestashop/fr/accueil/7-sweat-shirt-a-capuche-homme.html">sweat-shirt à capuche homme</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  26,62 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="7" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

    </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost/prestashop/fr/2-accueil">
    Tous les produits<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
